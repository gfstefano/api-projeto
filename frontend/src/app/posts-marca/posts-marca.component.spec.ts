import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostsMarcaComponent } from './posts-marca.component';

describe('PostsMarcaComponent', () => {
  let component: PostsMarcaComponent;
  let fixture: ComponentFixture<PostsMarcaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostsMarcaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostsMarcaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
