import { Component, OnInit, Input } from '@angular/core';
import { PostsService } from '../posts.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-posts-marca',
  templateUrl: './posts-marca.component.html',
  styleUrls: ['./posts-marca.component.scss']
})
export class PostsMarcaComponent implements OnInit {

  public posts: any = [];
  public marca: string;

  constructor(
    private service: PostsService,
    private router: Router,
    private route: ActivatedRoute
  ) {
    this.getPosts();
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.marca = params.parameter;
    });
  }

  getPosts() {
    this.service.getAllPosts().subscribe(
      result => {
        this.posts = result;
        console.log(this.marca);
        this.posts = this.posts.filter(post => post.marca === this.marca);
        console.log(this.posts);
      },
      error => {
        console.log('Error');
      }
    );
  }

}
