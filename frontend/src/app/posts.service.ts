import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class PostsService {

  public urlApi = 'http://localhost:3000/api/';

  constructor(
    private http: HttpClient,
    private router: Router,
  ) {
    console.log('Service');
  }

  getAllPosts() {
    console.log('GET ALL POSTS');
    return this.http.get(this.urlApi + 'posts');
  }

  getPostID(id) {
    return this.http.get(this.urlApi + 'posts/' + id);
  }

  deletePostID(id) {
    return this.http.delete(this.urlApi + 'posts/' + id);
  }

}
