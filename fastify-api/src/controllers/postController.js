// External Dependancies
const boom = require('boom')

// Get Data Models
const Post = require('../models/Post')

// Get all posts
exports.getPosts = async (req, reply) => {
  try {
    const posts = await Post.find()
    return posts
  } catch (err) {
    throw boom.boomify(err)
  }
}

// Get single post by ID
exports.getSinglePost = async (req, reply) => {
  try {
    const id = req.params.id
    const post = await Post.findById(id)
    return post
  } catch (err) {
    throw boom.boomify(err)
  }
}

// Delete a post
exports.deletePost = async (req, reply) => {
  try {
    const id = req.params.id
    const post = await Post.findByIdAndRemove(id)
    return post
  } catch (err) {
    throw boom.boomify(err)
  }
}
