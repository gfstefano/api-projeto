// External Dependancies
const mongoose = require('mongoose')

const postSchema = new mongoose.Schema({
  title: {
    type: "string"
  },
  link: {
    type: "string"
  },
  marca: {
    type: "string"
  },
  num_likes: {
    type: "string"
  },
  num_comments: {
    type: "string"
  },
  date: {
    type: "string"
  },
  description: {
    type: "string"
  },
}, {strict:false})

postSchema.set('collection', 'posts')

module.exports = mongoose.model('posts', postSchema, 'posts')
